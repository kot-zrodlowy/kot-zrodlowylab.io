---
layout: post
title: "Podstawowe założenia uczenia maszynowego"
---
Cześć! Jak wiecie, Kot ostatnio zgłębia całą masę wiedzy, w dużej mierze dotyczącej sztucznej inteligencji. Ten temat zawsze mnie ciekawił, nie tylko dlatego, że pobudza wyobraźnię twórców popkultury od dziesięcioleci. (Ktoś pamięta film 'Krótkie Spięcie'? To są lata 70. możecie mieć niezły ubaw przy oglądaniu) Ja jednak widzę w nim realny kierunek zmian, jakie zachodzą we współczesnym świecie. A skoro świat się zmienia, to można zrobić trzy rzeczy:

- Wyznaczyć kierunek tych zmian
- Reagować, gdy te zmiany już zachodzą
- Bronić się przed nimi rękami i nogami (a nawet ogonem), a potem psioczyć, że kiedyś to były czasy.

Najbardziej podoba mi się ta pierwsza opcja, a co za tym idzie, uczę się sporo, ciągle i taki stan zdaje się nie mieć końca. To bywa męczące, ale z drugiej strony, ja inaczej nie potrafię i nie chcę. 

Zacznijmy od początku. Jak ludzie często sobie wyobrażają uczenie maszynowe. Każdy, kto się kiedykolwiek spotkał z pojęciami związanymi ze sztuczną inteligencją wie, że chodzi o pracę z danymi - im więcej nasz system ich posiada, tym trafniejsze decyzje potrafi podjąć. Można to przyrównać do jakiegoś szamana minotaura z dowolnej gry RPG (nawet takiej papierowej). I ten oto szaman ma swój kocioł, w którym warzy magiczną miksturę. Specyfik pozwala odpowiedzieć poprawnie (z dużą dozą prawdopodobieństwa) na zadane pytanie. Więc nasz szaman dorzuca wszystko, cokolwiek znajdzie w swojej chacie, do mikstury i miesza w garze wielką chochlą. Tak, dane to wszystkie składniki, które wrzucamy do kotła, a powstała odpowiedź na każde pytanie, to wynik uczenia. Mam nadzieję, że rozumiecie analogię.

Dobra, porzućmy te bajki o sztucznej ineligencji jako kociołku Panoramiksa, a nawet buncie maszyn i scenariuszu pierwszego lepszego filmu o przyszłości. Bo w prawdziwym życiu, bez dokładnie przyszykowanych składników, odpowiednich proporcji i połączeń nie wyjdzie nam żadna magiczna mikstura, lekarstwo na wszystkie dolegliwości, ani nawet smaczne ciasto. To wcale nie jest tak, że bierzemy wszystkie dane, jakie mamy i nie bardzo wiemy, co z nimi zrobić, a potem uczymy nasz system, aby na wyjściu odpowiedzieć na każde pytanie świata. W rzeczywistości ciężar naszego myślenia nad rozwiązaniem problemu powinien być przerzucony na analizę danych, ich odpowiednie przygotowanie i pogłębioną analizę wniosków. Same algorytmy potrafią być bardzo proste (nie wszystkie) i intuicyjne. 

Dobra, bo ja na razie powiedziałam, czym ML nie jest, zamiast zacząć wyjaśniać na czym to polega. Tak naprawdę dzisiejszy wpis możesz traktować jako wstęp i zbiór wskazówek, w którą stronę iść dalej, żeby rozwijać się w tym konkretnym kierunku. Pamiętaj jednak, że sama ciągle się w tym temacie rozwijam, więc po więcej odeślę cię do źródeł, z których korzystam. 

### Zanim zaczniesz
Uczenie maszynowe, sztuczne sieci neuronowe i inne pokrewne dziedziny informatyki opierają się na nieco większym zakresie matematyki, niż przeciętny front-end dev zwykł stosować. Na podstawowym poziomie, to nie jest żadne rocket-science, ale jeśli rzut monetą nigdy nie był twoją mocną stroną, to tak naprawdę warto sobie przypomnieć podstawy statystyki i rachunku prawdopodobieństwa. Później dochodzi trochę rachunku na macierzach, ale to przecież nic na tyle strasznego, żeby sobie z tym nie poradzić. Ale nie ucz się za dużo na zapas, lepiej wziąć w łapki książkę tematyczną, na przykład [taką](https://www.packtpub.com/big-data-and-business-intelligence/hands-machine-learning-javascript) i zaległości nadrabiać w trakcie.

### To co dalej
Najlepiej zacząć od podstaw, czyli od zdefiniowania problemu, jaki chcemy rozwiązać. Zastanówmy się, co jest przyczyną, a co skutkiem, jakie czynniki mogły mieć wpływ na to, że skutek był taki, a nie inny. Jako przykład weźmy sobie bardzo tradycyjne doświadczenie losowe, jakim jest rzut kością sześciościenną. Tak naprawdę całkowicie losowe jest ono tylko na zadaniach z matmy. W rzeczywistości ma na to wpływ wiele czynników, jak na przykład wyważenie kości, siła rzutu, długość turlania się po stole, to którą ręką rzucasz, czy nawet to, którą stroną była obrócona, gdy wziąłeś ją do ręki. I to nie są tylko takie głupie pytania, a realne czynniki, które mogą mieć realny wpływ na wynik rzutu, a tym samym na jego przewidzenie. 

Jak dobrze wiesz, tradycyjna kość do gry ma 6 ścian, więc prawdopodobieństwo wyrzucenia każdej z nich wynosi 1/6 więc w przybliżeniu 0,16667; Jeśli dzięki naszym szacunkom będziemy w stanie przewidzieć chociaż trochę więcej rzutów niż tylko zwykły losowy przypadek, to już będzie sukces. Znaczy to mniej więcej tyle, że udało nam się wybrać te dane, które rzeczywiście mają wpływ na rzut.

### Gotowi? Cel? Testuj!
Gdy już uda nam się wybrać odpowiednie dane, to tak naprawdę musimy je przygotować. Zwyczajowo dzieli się je na dwa zestawy: 
- Treningowy - stanowiący bazę przykładów z rozwiązaniami, na których nasz algorytm będzie się uczył.
- Testowy, na którym będziemy sprawdzać skuteczność w przewidywaniu naszego algorytmu. 
Jak się do tego zabrać? Zbieramy dane i grupujemy je w odpowiednie struktury. Bardzo często tablica tablic okazuje się wystarczająca i łatwa z zarządzaniu. Problem z tablicą jest tylko jeden - przechowuje ona elementy w takiej kolejności, w jakiej je wpisaliśmy. Przed rozdzieleniem na dwa zestawy warto by je trochę wymieszać, żeby nie było, że nasz algorytm uczył się na zupełnie innych zestawach niż ma się testować (to na pewno nie wyjdzie). Gdy już rozdzielimy dane, to potem puszczamy nasz algorytm przez wszystkie przypadki treningowe dla jednego testowego. Gdy w ten sposób przerobimy przypadki testowe, możemy zliczyć dobre odpowiedzi i zobaczyć procentową skuteczność naszego aktualnego rozwiązania. 

### Normalizacja Danych
Pewnie to, co piszę dzisiaj brzmi trochę dziwnie, ale tak naprawdę algorytmów uczenia maszynowego jest wiele, a działania prowadzone na danych są podobne przy większości tych algorytmów. Chodzi o zrozumienie pewnych mechanizmów, żeby potem było łatwiej. Teraz przed nami chyba najbardziej techniczna sekcja. Załóżmy, że każda pojedyncza porcja danych składa się z trzech pozycji. 

``` js
[0.34, 12, 430]
```
I są to wspaniałe wartości liczbowe, niemniej problem jest jeden. Widząc taki zestaw, nie wiemy czy to dużo, czy mało. Każda dana jest podawana jakby w innej jednostce i zupełnie do siebie nie przystają. Musimy zatem zrobić coś, żeby je ujednolicić. I wiesz co? Idealnie nadają się do tego procenty. Jest taki specjalny wzór:
``` js
(obecnaWartość - minimalnaWartość) / (maksymalnaWartość - minimalnaWartość)
```
Oczywiście wartości do podstawienia pod minimalnaWartość i maksymalnaWartość bierzemy ze zbioru od najmniejszej i największej występującej wartości dla danej cechy. Innymi słowy musicie wiedzieć, jaka jest największa i najmniejsza użyta siła rzutu kostką we wszystkich próbach, jakie przeprowadziliście. W ten sposób dla każdej wartości uzyskacie jej reprezentację w zakresie liczb od 0 do 1 i będziesz mógł je bez obaw porównać.

## Ale po co to wszystko
No niby wszystko fajnie brzmi, głowa pracuje, para bucha, ale może zastanawiasz się do czego AI może się przydać po stronie przeglądarki? Przecież ty chcesz tylko robić stronki. Tylko wiesz, ludzie robią stronki dla klienta, czytelnika, cały kontent w internecie nie miałby sensu, gdyby nie było nikogo, kto tam zagląda. Koty nie byłyby królami wszechświata, gdyby nie było ich skromnych poddanych, prawda? Więc ludzie mają się czuć dobrze, wchodząc na twoją stronę. Co ty na to, żeby stronę pokazywać w twojej ulubionej kolorystyce? Albo jeśli zwykle używasz trybu nocnego, w pewnych konkretnych porach, ten mógłby się włączać automatycznie po zanalizowaniu wystarczająco wielu przypadków podobnych do twojego. Zastosowań jest wiele, mamy czwartą rewolucję przemysłową i możemy być jej częścią, wyznaczać nowe trendy, albo tylko biernie się przyglądać. 
Niech przyszłość będzie z Tobą!
Miau!
---
layout: page
title: Współpraca
permalink: /cooperation/
---

Cześć! 
Trafiłeś do jedynego w swoim rodzaju miejsca, gdzie kocimiętka wysypuje się z każdego kąta, a nauka kodowania idzie sprawnie i szybko. Prawda jest taka, że artykuły na tym blogu pomogły już niejednemu kociakowi na drodze do zostania prawdziwym tygrysem programowania. 

Skoro tu jesteś, to znaczy, że chcesz zrobić ze mną coś fajnego. Kot lubi fajne rzeczy i przygody (nawet jeśli przez nie można się spóźnić na podwieczorek). Jednak nade wszystko kot pała sympatią do swoich czytelników i o tym nie wolno zapomnieć także i tobie. 

Jeśli chcesz zrobić razem coś fajnego, to po prostu napisz do mnie! Ja nie gryzę, a biorąc pod uwagę moją wrodzoną kreatywność, na pewno będziesz zadowolony po takiej współpracy. Co możemy zrobić? To zależy, czego tak naprawdę oczekujesz, ale jestem otwarta na twoje propozycje. Możemy umówić się na wpis gościnny, sponsorowany, konkurs, recenzję i co tylko przyjdzie ci do głowy. Z chęcią również wystąpię na organizowanym przez ciebie meet-upie, konferencji, webinarze lub warsztatach. Masz pomysł, który wykracza poza wszelkie ramy świadomości? Śmiało napisz, a ja zobaczę, co da się zrobić. 

mail kontaktowy: kontakt@kot-zrodlowy.pl

Pozdrawiam

Kot Źródłowy

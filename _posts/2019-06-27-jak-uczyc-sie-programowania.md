---
layout: post
title: "Jak uczyć się programowania? - Q&A"
category: offtop
---

Cześć,
dawno mnie nie było. No cóż, praca, praca… Nagle okazuje się, że czas jest najcenniejszym zasobem. Również moje pokłady kreatywności często są wykorzystywane do zupełnie innych celów. Ale nie ma się co martwić, wracam z nowym blogowym eksperymentem. Odpowiem na kilka pytań od adeptów sztuki programowania, które przewinęły się przez internet. Ludzie, którzy je zadali, byli raczej na początku, czyli w punkcie, w którym każdy z nas się kiedyś znajdował. Pamiętajcie, że nie ma głupich pytań – z takim nastawieniem piszę tego posta i o taką samą postawę proszę ciebie. No to zaczynamy!

### Ile książek trzeba przeczytać, żeby dostać pierwszą pracę?

Ostatnio też zamiast książek możesz wpisać kursy wideo, które stają się coraz popularniejsze. Prawda jest taka, że żadna książka nie gwarantuje ci zatrudnienia. No bo pomyśl sobie, ile książek trzeba przeczytać, żeby zostać kierowcą rajdowym? Albo chirurgiem? 

I zdaję sobie sprawę, że tu sytuacja jest trochę inna niż w zawodzie chirurga – w większości przypadków nie potrzebujesz formalnego wykształcenia. Z tym, że twój przyszły pracodawca musi mieć jakiś dowód na dwie rzeczy: 

1. Coś już potrafisz wykonać samodzielnie i masz to jakoś udokumentowane (choćby projektem na GH).

2. Potrafisz zdobywać wiedzę na własną rękę i nie musisz być prowadzony za rączkę. 

I nie zrozum mnie źle, w pracy nauczysz się bardzo dużo. Zespół zazwyczaj chętnie dzieli sie wiedzą, ale kiedy chcesz zostać kierowcą rajdowym, to prawo jazdy przydałoby się zdać samodzielnie.

### Czy żeby nauczyć się programować, muszę robić to codziennie?

To pytanie bardziej o techniki nauki czegokolwiek niż programistyczne rozterki. Pozwól, że odpowiem na nie, biorąc pod uwagę obie strony zagadnienia. Po pierwsze, styl nauki. Czyli pytania, czy uczyć się codziennie, w jakiej kolejności to robić, jakie zagadnienia są bardziej potrzebne i pożądane na rynku pracy? Tutaj nikt ci na to poważnie nie odpowie, bo mózg każdego z nas działa trochę inaczej. I znaczna część technik, typu czytanie tego samego po kilka razy, nie działa. Z tych działających mogę polecić tylko tyle, że programowanie wchodzi do głowy przez palce, a nie przez oczy. Jeśli chcesz dowiedzieć się więcej o mądrych sposobach uczenia się, to mogę ci polecić jedną pozycję książkową: 
[Włam się do Mózgu](https://altenberg.pl/radek/) autorstwa Radka Kotarskiego. Przeczytałam, stosuję i uważam, że jest fenomenalna.

Jeśli chodzi o drugą stronę, to trochę faktów na temat samego programowania. Na samą czynność programowania składają się dwa rodzaje umiejętności:

1. Składnia i gramatyka języka programowania

2. Umiejętność rozwiązywania problemów w sposób zrozumiały dla komputera.

Mam wrażenie, że wielu początkujących programistów skupia się tylko na tym, aby nauczyć się składni języka, a później nowego frameworka czy jakiegoś innego modnego rozwiązania. Owszem problem-solving przychodzi później, ale jest to już umiejętność, której nie pokaże ci żaden kurs. Jasne, są takie źródła, które pokażą ci na przykładach, jak podchodzić do najczęściej spotykanych problemów, ale nie nauczysz się tego robić samodzielnie, jeśli nie zaczniesz próbować. Możesz mieć najlepszy kurs na świecie, po skończeniu którego w twoim portfolio znajdzie się kilka zgrabnych projektów. Jednak robienie rzeczy krok po kroku z wideoporadnikiem nie da ci takich umiejętności, jakie zapewnia stworzenie czegoś od a do z samodzielnie.

### Chcę zająć się GameDevem ale słyszałem, że frontend jest łatwiejszy. Co polecacie na start?

Proszę cię, nawet o tym nie myśl. Zmarnujesz pół roku na poznanie podstaw technologii, którą potem porzucisz, żeby poznać podstawy właściwej technologii. Chcesz wiedzieć dlaczego to jest bez sensu? Pomimo że zasada działania wielu języków programowania jest bardzo zbliżona, logika jest jedna dla wszystkich, to angażując się w poznanie różnych technologii spotkasz się z zupełnie innymi problemami do rozwiązania. I tak w C++ spotkasz się z ręcznym zarządzaniem pamięcią, statycznym typowaniem zmiennych, za to w JS te problemy w ogóle nie występują. Sen z powiek wielu początkujących spędza z kolei hoisting, koercja czy `this`. Nie uważam też, żeby warto było wybierać technologię tylko dlatego, że jest łatwiejsza od innej. To może być pozorne, gdyż ogólne zasady są zbliżone w wielu technologiach, a reszta to kwestia wprawy i przyzwyczajenia. 

To wszystko na dzisiaj. Mam nadzieję, że w miarę moich możliwości wyczerpałam temat. Masz jeszcze jakieś pytania? Może za jakiś czas zrobię następny wpis. Powodzenia i uważaj na klimatyzację.
Miau! 

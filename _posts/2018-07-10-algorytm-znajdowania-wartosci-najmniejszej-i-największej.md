---
layout: post
title: Algorytmy - Wyszukiwanie najmniejszego/największego elementu.
categories: algorytmy javascript
---

Cześć! Wracam dzisiaj z algorytmem. Algorytmy są fajne, a jeden z nich przyszedł mi na myśl, kiedy zobaczyłam wpis o *absurdach* w JS. Na ten temat jeszcze napiszę, więc bez obaw. Nie bronię tego języka jako jedynego słusznego, ale wydaje mi się, że nazywanie absurdami pewnych mechanizmów wynika z niewiedzy. 

Jednak nie o tym dzisiaj mowa. Wiecie dobrze, że lubię algorytmy. Jednym z najprostszych jest wyszukiwanie najmniejszego/największego elementu w tablicy. Zasada jest dokładnie taka sama. Doskonale zdaję sobie sprawę, że do takiego przeszukiwania istnieją funkcje wbudowane, ale warto wiedzieć, co tam pod spodem się dzieje. Czasami nawet z czystej ciekawości. Bo to właśnie ten rodzaj ciekawości nas pcha do przodu i dzięki temu się rozwijamy. 

## Problem

Załóżmy, że mamy tablicę złożoną z liczb i naszym zadaniem jest znaleźć najmniejszy element występujący w tej tablicy. Zaczniemy od prostego kodu.

```js
const myNumbers = [2, -1, -634, 234, -12.3242, 0, 1020];
findMin(myNumbers); // -634
```

Oczywiście funkcję `findMin` napiszemy niebawem samodzielnie. Przyjmie ona na wejściu tablicę złożoną z liczb, natomiast wyjściem będzie najmniejszy element naszej tablicy. 

## Algorytm

Jeśli się jeszcze nie ma wprawy w algorytmach, warto je sobie zapisywać słownie w postaci listy kroków. Tak jest naprawdę łatwiej zobaczyć, czy o niczym nie zapomnieliśmy. W jaki sposób sprawić, żeby algorytm wiedział, że wybrał najmniejszą wartość? Będziemy iterować po tablicy i porównywać wartości. Jeśli trafimy na mniejszą, to ją wybieramy i tak aż do skutku. Już piszę konkretniej.

1. Stwórz zmienną `min` i przypisz jej dużą wartość. Wartość ta zależy tak naprawdę od tego, jakie liczby chcemy porównywać. Musi być większa niż elementy naszej tablicy wejściowej, żeby działało. Chcesz mieć najbardziej uniwersalną funkcję na świecie? Przypisz tam wartość `Infinity`.

2. Weź pierwszy element tablicy. 

3. Porównaj go do naszej wartości `min`. 

4. Jeżeli element tablicy jest mniejszy niż wartość `min`, to przypisz wartość elementu tablicy do zmiennej `min`. 

5. W innym wypadku przejdź do następnego elementu tablicy.

6. Powtarzaj, dopóki nie skończysz całej tablicy.

W ten sposób tablica zostanie przeiterowana, a jej najmniejszy element będzie zapisany do zmiennej `min`. Nie wygląda to specjalnie skomplikowanie, prawda? To bierzemy się za kodowanie naszej funkcji.

```js
function findMin( numbers ) {
	let min = Infinity;
	numbers.forEach( ( elem ) => {
		min = ( elem < min ) ? elem : min; 
	});
	return min;
	
} 
```

W zupełnie analogiczny sposób znajdziesz maksymalną wartość tablicy. I kot naprawdę wierzy w twoje umiejętności i myśli, że nie musi ci pokazywać kodu. Za to ty możesz się nim pochwalić w komentarzu. Disqus obsługuje kolorowanie składni poprzez dodanie:

```html
<pre><code class="javascript" >
 <!-- Możesz umieścić swój kod pod tym komentarzem -->
</code></pre>
```

Na dzisiaj to wszystko. Baw się dobrze w te wakacje!
Miau
---
layout: post
title: Jak zmigrować swoje projekty i strony na GitLaba
categories: programowanie opensource
data: 2018-06-06 15:00 +0100
---

Cześć kociaki, to pierwszy artykuł po zmigrowaniu bloga na serwis Gitlab. Mam nadzieję, że nie było widać znaczących przestojów ani większych problemów z tym związanych. Kto mnie obserwuje na [Kocim Fesjbuku](https://www.facebook.com/kotzrodlowy/), ten miał możliwość śledzenia procesu na bieżąco. Jeśli ktoś poczuł się urażony nadmiarową aktywnością, to przepraszam (jako zadośćuczynienie mogę ci wysłać miseczkę mleczka – ale to wymaga szybkiego łącza, więc dobrze się zastanów). Kilkoro z was chciało się zapoznać z procesem migracji, więc niniejszym spełniam waszą prośbę (bo jestem takim kochanym kotem). Decyzję, czy postąpić tak jak ja, pozostawiam wam, a i nie mam zamiaru za bardzo wdawać się w dyskusje, dlaczego to zrobiłam.  

## Zanim zaczniesz

Gdyby ktoś zapytał mnie, od czego zacząć – powiedziałabym, że od porządków. To dobry czas, żeby popatrzeć na swoje repozytoria i ogarnąć, czy może jakieś są ciągle aktualne, czy chcielibyśmy się którychś pozbyć albo coś w tym stylu. Oczywiście, jeśli kochasz wszystkie swoje repozytoria jak własne dzieci, nie rób nic – tak też możesz. 

Jeśli nie masz jeszcze konta w serwisie GitLab, to najwyższy czas, żeby je założyć. Na pewno poradzisz sobie ze standardową procedurą. Wierzę w ciebie!

Zrobiłeś to? Brawo Ty! Sprawdź jeszcze, czy aktywowałeś konto za pomocą linku w mailu. Teraz musisz jakoś połączyć swojego GitHuba z GitLabem. Masz dwa wyjścia. (Jeśli zarejestrowałeś się na GitLabie za pomocą konta GitHub, to możesz ominąć tę sekcję)

### Łączenie konta GitHub z GitLab

To jest to łatwiejsze rozwiązanie. Postaram się przeprowadzić cię przez nie krok po kroku.

1. Wejdź w ustawienia swojego profilu (**Settings**).
2. Z menu po lewej wybierz konto (**Account**).
3. W sekcji social sign-in wybierz GitHub i kliknij **Connect**.
4. Postępuj zgodnie z instrukcjami na ekranie.

Prawda, że proste? A teraz coś dla prawdziwych Jedi.

### Używanie GitHub tokena

1. Przejdź do [https://github.com/settings/tokens/new](https://github.com/settings/tokens/new).
2. Opisz jakoś swój token.
3. Wybierz scope: **repo**.
4. Wygeneruj token.
5. Skopiuj hash tokena.

## Kopiowanie repozytoriów

1. Wejdź na stronę główną [GitLaba](https://gitlab.com/). 
2. Kliknij **New Project**.
3. Klinkij zakładkę **Import Project**.
4. Wybierz **GitHub**.
5. Jeśli wybrałeś metodę z tokenem – wpisz go w odpowiednie miejsce.
6. Powinny ci się pokazać grupy, w których jesteś i masz dostęp do repozytoriów. Zaznacz wszystko, czego ci potrzeba. 
7. Pokaże ci się lista wszystkich repozytoriów – możesz zaznaczyć wybrane lub kliknąć u góry **Import All Repositories**.

Niestety, z tego co zauważyłam, grupy i przypisane do nich repozytoria trzeba będzie stworzyć na nowo. Wydaje mi się też, że na GitLabie standardowo wszystkie projekty są ustawione jako prywatne, ale można to łatwo zmienić. Jeśli pracujesz tylko z kodem – tutaj twoja praca się kończy. Możesz teraz ustawić sobie CI i całą masę innych bajerów, co wygląda, brzmi i działa fajnie. 

## Konfiguracja strony GitLab

Jeśli chcesz postawić swoją stronę statyczną lub zbudowaną za pomocą jakiegokolwiek generatora stron (np. Jekyll) zahostować na GitLabie, to uwierz, że to jest naprawdę proste! Potrzebujesz repozytorium z kodem i kilku dodatkowych chwil. Repozytorium nazwane wg wzoru

```http
namespace.gitlab.io
```

gdzie namespace to twoja nazwa użytkownika lub grupy, jeśli repo należy do grupy, będzie na domenie o tej samej nazwie. To tak zwana strona użytkownika lub strona grupy. Jeśli nazwiesz swoje repo  jakkolwiek inaczej, to możesz stworzyć stronę projektu, która będzie dostępna pod adresem:

```http
namespace.gitlab.io/reponame
```

Jak to mówią mistrzowie gry, wybierz mądrze. Zarówno nazwę każdego projektu, jak i repozytorium (a co za tym idzie adresu) możesz zmienić w ustawieniach repozytorium. Nazwy projektu szukaj w `project -> settings -> general`. Zmiana nazwy repozytoruim znajduje się w ustawieniach zaawansowanych czyli: `project -> settings -> advanced`. 

### Konfiguracja silnika

1. Wejdź na stronę swojego projektu, czyli pod adres: 

```http
gitlab.com/<username>/<project>
```

2. Kliknij **Set Up CI/CD**
3. Wybierz template dla pliku `.gitlab-ci.yml` i wyszukaj swój generator stron statycznych (u mnie Jekyll).
4. Kliknij **Commit changes**.
5. Poczekaj aż zmiany się zbudują.
6. Wejdź do ustawień: `project -> settings -> pages`
7. Możesz tu poustawiać dodatkowe rzeczy dla swojej strony.

Po zakończeniu budowania w zakładce pages w ustawieniach pojawi ci się adres twojej strony.

To już wszystko na dzisiaj. Jeśli cokolwiek byłoby niejasne, nie obawiaj się zapytać o to w komentarzu tutaj, na fanpejdżu lub w wiadomości prywatnej. W miarę możliwości pokieruję cię do innych źródeł lub podzielę się swoją wiedzą tajemną. Trzymaj się ciepło i baw dobrze – w końcu niedługo wakacje.

Miau!


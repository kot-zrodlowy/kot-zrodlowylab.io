---
layout: post
title: Różne interfejsy użytkownika pisane JS-em
categories: javascript programowanie
date: 2019-04-02
---
Cześć, to znowu ja. Pamiętacie może, że ten blog miał pokazać, że w JS można zaimplementować niemal wszystko, czego dusza zapragnie? Nie? To muszę wrócić do sławnej i wspaniałej serii zwanej “50 Twarzy JS”. Pisanie tego było nawet zabawne, chociaż bardzo czasochłonne. Dzisiaj pochylimy się nie nad superhipernowoczesnymi możliwościami różnych platform, a nad czymś tak oczywistym jak interfejs użytkownika. Pamiętajcie, że tak naprawdę programując cokolwiek, robimy to dla jakiegoś użytkownika. On będzie z tego korzystał i komunikował się z “bebechami” naszej aplikacji, żeby osiągnąć jakieś swoje cele. Jakie? Wszystko zależy od tego, jaką aplikację tworzysz. My dzisiaj jednak nie o tym. Sam musisz wiedzieć, o czym chcesz pisać. Ja dzisiaj spróbuję odrobinę poopowiadać o tym, co charakteryzuje dany interfejs, jakie są jego współczesne zastosowania, a także pokażę przykład biblioteki ułatwiającej pisanie tej rzeczy w JS. Zaczynamy!

Aby zrozumieć w pełni możliwości i korzyści płynące ze stosowania interfejsów użytkownika, musielibyśmy się cofnąć do czasów początków komputeryzacji wspieranej taśmami perforowanymi, kiedy to taka maszyna była obsługiwana przez wykwalifikowany personel. Użytkownik wtedy się komunikował w sposób możliwie jak najbardziej zrozumiały dla komputera. Od tych czasów minęly dekady i sytuacja się odwróciła, dzisiaj to komputery dostosowują się do naszych potrzeb, a wszystko, co robimy, ma być coraz bardziej intuicyjne. Spokojnie, nie mam zamiaru przedstawiać całej historii komputerów, gdyż większości z was ta wiedza przyda się w minimalnym lub żadnym stopniu. Chciałam tylko nadmienić, że wraz ze wzrostem popularności komputerów osobistych, rośnie nam poziom abstrakcji oprogramowania, więc nasze interfejsy stają się coraz bardziej intuicyjne i ludzkie. Ta tendencja będzie sie utrzymywać, niemniej nie mnie oceniać, w którą stronę pójdziemy. Może skończymy na przekazie myśli? Tego nie wiem, ale musimy pamiętać, że nawet najlepszy interfejs na nic nam się zda, jeżeli aplikacja tak naprawdę nie ma nic do zaoferowania.

### Interfejs tekstowy (TUI)

Wykorzystywany powszechnie przed erą okienek i myszek, obecnie niemal zapomniany. Jednak zdarzają się sytuacje, kiedy ciągle się przydaje. Dość popularnym przykładem niech będzie Grub – tak, to jest ten ekran, w którym możemy włączyć Linuxa w trybie awaryjnym lub wybrać jedną z kilku zainstalowanych instancji. W codziennym życiu wolimy jednak posługiwać się interfejsem graficznym, dlatego ten pierwszy przykład podaję jako ciekawostkę. Sama nie miałam nigdy potrzeby budowania go, ale dla ciekawskich znalazłam taką oto bibliotekę: [blessed](https://github.com/chjj/blessed).

### Interfejs wiersza poleceń (CLI)

To już jest trochę lepiej znane, szczególnie developerom oprogramowania. Na pewno nie raz już robiłeś operacje typu:
``` sh
npm install
git commit
create-react-app myApp
```
To jes właśnie wiersz poleceń albo inaczej terminal. W tym układzie to użytkownik musi wiedzieć, jakie polecenie wpisać, a komputer je wykonuje lub ewentualnie zwraca błąd. Jest to bardzo stary sposób komunikacji z komputerem, niemniej jednak ciągle wykorzystywany. Pewnie zapytasz, dlaczego taki relikt przeszłości ciągle jeszcze żyje i jest w użyciu. Przede wszyskim, kiedy spamiętasz te polecenia, to jest on o wiele szybszy do zaaplikowania. Poważnie, nie musisz odrywać rąk od klawiatury, żeby ogarnąć wszystkie potrzebne działania. Po drugie, bardzo łatwo jest go zautomatyzować. Zapisujesz ciąg poleceń do odpowiedniego pliku i już na dobrą sprawę masz prosty skrypt, który możesz wykorzystać do jeszcze większego przyspieszenia swojej pracy. Po trzecie, kiedy mamy różne poradniki okołoprogramistyczne, dużo prościej po prostu przekazać zaklęcia do przekopiowania niż pokazać, jak coś się wyklikuje graficznie. Same plusy, a tylko trochę wysiłku. I mówię wam to nie tylko dlatego, że jestem maniakiem konsoli. 
A teraz szybkie rady dla twórców takich interfejsów. Może akurat brakuje ci jakiegoś odjazdowego narzędzia do pisania swojego superkodu lub masz tuzin innych pomysłów, gdzie interfejs linii poleceń będzie najlepszym wyjsciem. 

- Zadbaj o czytelny system pomocy. Ważne, żeby wyświetlał sie także, gdy użytkownik wpisze nieprawidłowe polecenie. To taka naturalna konwencja, która pozwala od razu się poprawić.
- Tutaj często nie ma miejsca na przesadną kreatywność. Im bliżej interfejsowi twojego programu do ogółu programów konsolowych, tym lepiej. Pamiętaj, twój użytkownik mniej rzeczy musi się wtedy uczyć na pamięć.
- Wyświetlaj informacje o błędzie ilekroć działanie programu zawiedzie. Wtedy powinieneś też zatrzymać działanie programu. Nieraz widziałam skrypty sypiące błędami jak z rękawa, na końcu których pojawiał się zapis: “Done without errors”.
- Postaraj się o wyświetlanie jakiejkolwiek animacji, jeśli jakaś akcja wykonuje się zauważalnie dłużej. Dlaczego? To proste, wtedy użytkownik wie, że komputer mu się nie zawiesił, tylko rozpracowuje jakiś problem.
Narzędzi do pomocy przy tworzeniu tego typu aplikacji jest mnóstwo, jednak ja nie mogłam pominąć jednego - [meow](https://www.npmjs.com/package/meow).

### Interfejs graficzny (GUI)

Obecnie jego istnienie zaciera się z interfejsem webowym, więc pozwolę sobie je traktować jako jeden typ. I o ile nie mam zamiaru podawać żadnego frontendowego frameworka (bo naraziłabym się pozostałej części community), to moim ukochanym desktopowym maleństwem jest [electron](https://electronjs.org/).

Zdaję sobie sprawę, że JS jest szeroko utożsamiany z tak zwanym front-end developmentem, co w praktyce oznacza implementację graficznych interfejsów w aplikacjach sieciowych. Ale naprawdę to jest tylko kropla w morzu zastosowań. Cieszę się, że powoli się odchodzi od stereotypu programisty JS jako człowieka, który wrzuca animowane kursory na stronę. Oczywiście sam frontend stwarza ogromne możliwości rozwoju zarówno programisty, jak i oprogramowania. Przeglądarka jest już osobnym ekosystemem i można z niej naprawdę wiele wycisnąć. Tylko ja idę o krok dalej. Zrywam ze stereotypem, który mówi o programiście JS jako o front-end developerze. Bo tak naprawdę to możliwości tego języka są o wiele większe niż tylko oprogramowanie interfejsów graficznych.

A co do tego, wiem, że zasad projektowania i programowania takich interfejsów jest mnóstwo. Zdaję sobie sprawę z różnych nurtów w designie, tego, że tutaj jest bardzo dużo mody i potrzeby wyróżnienia się, zapadania w pamięć. Niejednokrotnie design strony jest wizytówką firmy. Nie można jednak w tym całym kociokwiku pt. "Wyróżnij się albo zgiń" zapomnieć o najważniejszym: ktoś tego wszystkiego będzie używał. I dla mnie, z perspektywy użytkownika, pewne schematy są po prostu dobre. Nie muszę wtedy się zastanawiać, co robić dalej, żeby spełnić swoje cele. A skoro nie muszę myśleć, to czuję się bezpiecznie, a jak się czuję bezpiecznie, to zostaję na dłużej i wracam chętniej. Profit. Wszyscy lubimy rzeczy ładne, ale musimy pamiętać o użyteczności naszych rozwiązań. Tak naprawdę na ten temat mógłby powstać nie tylko osobny wpis, ale też książka. I powstała niejedna, bo to naprawdę ciężki temat. Od siebie mogę polecić [artykuł gościnny](https://kot-zrodlowy.pl/goscinne/programowanie/2017/09/11/najczeste-bledy-poczatkujacych-i-nie-tylko-webdeveloperow.html), napisany tu kiedyś przez Comandeera. Myślę, że to całkiem dobry start.

Myślę tylko, że warto tu nadmienić specjalny typ interfejsu graficznego, zwany interfejsem dotykowym, gdzie urządzenie wyjścia jest jednocześnie urządzeniem wejścia. Tutaj bardzo ważne jest, aby wszystko, co ma być widoczne, było widoczne, a wszystko, co można pacnąć, dało się pacnąć. Elementy nie mogą być zbyt małe ani źle się prezentować po wysunięciu klawiatury. Myślę sobie, że to już materiał na osobny wpis.

### Interfejs Ruchowy (Gestowy)

Obecnie wykorzystywany szeroko w konsolach do gier i kontrolerach wirtualnej rzeczywistości. Znalazłam kilka przykładów wykorzystania tego cuda, np. podczas prezentowania za pomocą [reveal.js](https://github.com/willy-vvu/reveal.js). Warto też nadmienić, że znalazłam kawałek kodu, który w zamyśle można wykorzystać do rozpoznawania gestów za pomocą kamerki – [LeapTrainer.js](https://github.com/roboleary/LeapTrainer.js).

Niemniej to już jest chyba wyższa szkoła jazdy. Rozpoznawanie obrazów to osobna, szybko rozwijająca się dziedzina w IT, ale widzę już kilka potencjalnych zastosowań. A ty, do czego byś wykorzystał rozpoznawanie tego typu gestów w JS?

### Interfejs konwersacyjny (najczęściej głosowy)

Kolejna bardzo nowa dziedzina wiedzy wspomagana sztuczną inteligencją. Zupełnie nowy sposób porozumiewania się z użytkownikiem i inna jakość doświadczenia. W zamyśle ma być tak naturalne dla człowieka, jak się tylko da. O czym ja właściwie mówię? O botach i asystentach głosowych. Jakiś czas temu świętowaliśmy start Asystenta Google w naszym kraju. Uważam go za naprawdę przydatne narzędzie. Niemniej jednak na tym świat się nie kończy, bo różne bardziej lub mniej inteligentne boty wkradają się do wielu dziedzin naszego życia. Sama lubię z nich korzystać i myślę, że w pewnych wypadkach stanie się to standardem tak naturalnym, jak wysłanie wiadomości na Messengerze. Jedno mnie tylko zastanawia. Każdy asystent głosowy ma jakąś swoją osobowość, a z przyjemnością widziałabym jakieś nakładki zmieniające asystenta chociażby w doradcę z gry Stronghold. 

Przykładowy dialog:
JA: Co mam dziś do zrobienia?

Wybrany Asystent Głosowy: Pani, spichlerz jest pusty, czas zrobić zakupy.

I ponownie, projektowaniu interfejsów konwersacyjnych poświęca się teraz dużo uwagi, a dobre praktyki tworzą się na naszych oczach. Na pewno powstanie na ten temat jeszcze niejeden wpis, bo w tej kwestii dokształcam się cały czas. Jedyne, co mam ci do przekazania w tym momencie, to właśnie zmiana sposobu myślenia. To nie są oddzielne byty napakowane nie wiadomo jakimi ficzerami, tylko właśnie kolejny rodzaj interfejsu pozwalający nam jeszcze wygodniej spełniać nasze cele. I co ciekawe, popularne asystenty głosowe mają SDK pozwalające na rozszerzanie ich możliwości za pomocą JS-a. Nie wiem, co z tym faktem zrobię, ale będzie to wymagać ode mnie długiego riserczu. A co wy chcielibyście załatwiać za pomocą botów i asystentów?

### Podsumowanie

I to by było na tyle, jeśli chodzi o dzisiejszy wpis. Mam nadzieję, że wam się spodobał i zainspirował do szukania nowych ścieżek rozwoju. Podsumowując, obecnie mamy wiele dróg komunikowania się z użytkownikiem, czyli interfejsów. Jesteśmy świadkami odchodzenia w niepamięć jednych i rodzenia się nowych. Żaden z nich nie pomoże nam, jeśli nie będziemy mieli nic ciekawego do przekazania naszym użytkownikom. Jeśli już decydujemy się na zaprojektowanie i wdrożenie jakiegoś w naszej aplikacji, to musimy wiedzieć jedno. Użytkownik jest tu najważniejszy, a interfejs musi być przede wszystkim pomocny. To całkiem dużo wiedzy, jak na taki jeden mały wpis. Pozdrawiam i ściskam mocno.
Miau!
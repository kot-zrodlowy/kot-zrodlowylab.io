---
layout: post
title: "Wprowadzenie do Komponentów Webowych"
categories: javascript
---
Cześć,
Ostatnio zainteresowałam się tematyką natywnych komponentów webowych i chciałam się z tobą podzielić swoimi spotrzeżeniami. Dzisiaj ogólnie, trochę teorii, a w kolejnych wpisach zmierzę się z procesem stworzenia i publikacji własnego komponentu. Dla porządku postaram się też je podlinkować tutaj.

### Czym właściwie są Web Komponenty?
Tak najprościej rzecz ujmując, to przeglądarka udostępnia nam zbiór API, który umożliwia tworzenie własnych elementów strony. Mają one swój znacznik i strukturę (HTML), wygląd (CSS) oraz działanie (JS). Jednocześnie są odpowiednio *zhermetyzowane*, czyli zamknięte w gotowe paczki, których kod nie powinien się pogryźć z kodem całej aplikacji. Jeżeli znasz jakiś framework, to z pewnością wiesz, o jakim podejściu mówię. W Reakcie oprogramowujesz działanie każdego komponentu dokładnie tak, jak masz na to ochotę, a potem z takich komponentów składasz gotową aplikację.

Różnic pomiędzy natywnymi komponentami a gotowym frameworkiem jest kilka. Po pierwsze, do podstaw nie potrzeba nam żadnych narzędzi oprócz przeglądarki i edytora. Żadnych bundlerów, transpilerów i tym podobnych cudów. Można usiąść, po prostu zacząć wymyślać i pisać kod. To moim zdaniem ogromna zaleta, gdyż nie tracisz czasu i zapału na poznawanie narzędzi, zamiast po prostu zacząć pisać. Po drugie, są całkiem nieźle wspierane przez współcześnie rozwijane przeglądarki, więc gdy nie musimy wspierać trupów, to droga wolna. [Pełna tabela wsparcia](https://caniuse.com/#search=components) jest różna dla różnych składowych API. Poza tym, te komponenty pod pewnymi warunkami można stosować w znanych frameworkach, o czym też może kiedyś napiszę.

### API Komponentów Webowych
API rozdziela się na kilka składowych, zależnie od zadań, jakim musimy sprostać. Strona [webcomponents.org](https://www.webcomponents.org/) podaje cztery składowe, natomiast [MDN](https://developer.mozilla.org/en-US/docs/Web/Web_Components) tylko trzy. Gdyby ktoś pytał o zdanie kota, to też mi się wydaje, że czwarty element jest naciągany, ale tak naprawdę to nie ma większego znaczenia. Sedno problemu leży zupełnie gdzieś indziej. Pozwolisz, że krótko opiszę, na czym polega dane API, a potem odeślę cię do stosownych źródeł. Nie chcę, abyś musiał spędzić wieczność na czytaniu moich artykułów. Jakkolwiek ciekawe by one nie były, to po prostu szkoda życia.

#### Custom Elements
Gdybym miała wskazać najważniejszy kawałek, to z pewnością postawiłabym na ten. To jest API, które pozwala nam stworzyć swój element HTML. Tworzymy je za pomocą JS-a, który potem includujemy i dodajemy do naszego HTML. Jak zacząć? Zupełnie prosto.
``` js
class <nazwa-klasy> extends HTMLElement {
	constructor() {
		super();
	}
}

customElements.define('<nazwa-znacznika>', <nazwa-klasy>);
```
A później w dokumencie HTML użyć tego.
``` html
<nazwa-znacznika></nazwa-znacznika>
```
I to zupełnie tyle. Co robi nasz element? Absolutnie nic. Jak wygląda? No, nie wygląda, wszystkim dopiero musimy się zająć. Ale to innym razem.
A teraz kilka zasad przy tworzeniu:

* Nazwa znacznika musi zawierać myślnik. Inaczej przeglądarka nie wie, czy nie szukać jakiegoś standardowego elementu.
* Raz zarejestrowany element nie może być rejestrowany ponownie ani edytowany.
* Custom element nie może być samozamykający się, jak np. tag `<img/>`.

#### HTML template
Jest to po prostu tag `<template></template>`, który pozwala na zrobienie kawałka HTML-a, który nie jest wykonywany od razu, ale dopiero po wywołaniu w kodzie. Możemy za jego pomocą stworzyć szablon do dynamicznego generowania zawartości. Całkiem przyjemny, jeżeli chcemy nasz komponent złożyć z kilku elementów HTML-a.

#### Shadow DOM
Jest to osobny DOM, który możemy dołożyć do naszego komponentu w celu enkapsulacji stylów i skryptów. Dobrze wiesz, że w tradycyjnym podejściu w sieci wszystko jest globalne. Przestaje, gdy tylko zainteresujesz się tą technologią. Pozwoli to uprościć CSS-y, kod JS, a także uniknąć różnych dziwnych konfliktów.

To co najważniejsze już za nami. Nie martw się, jeżeli masz w głowie mętlik. Z pewnością warto przeczytać artykuły poniżej:

* [webcomponents.org](https://www.webcomponents.org/introduction)
* [MDN](https://developer.mozilla.org/en-US/docs/Web/Web_Components)
* [Google Developers](https://developers.google.com/web/fundamentals/web-components/customelements)

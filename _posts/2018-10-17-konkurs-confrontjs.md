---
layout: post
date: 2018-10-17
title: Konkurs - Kot Źródłowy na ConFrontJS - wygraj bilet.
categories: javascript offtop
---
Cześć! To znowu ja! Wiem, że szybko i w ogóle. Ale z takimi informacjami działa się ASAP. Kot dostał darmowy bilet na ConFrontJS, a że został patronem, to ma darmową wejściówkę dla jednego z was. Nie będę się rozpisywać co i jak, bo szczegółów dowiecie się ze [strony organizatora konferencji](https://confrontjs.com/#partners), a także z wyczerpującego [wpisu Comandeera](https://blog.comandeer.pl/na-luzie/2018/10/17/konferencja-confrontjs.html). Kot jest za bardzo zaaferowany ogarnianiem własnych pudełek (przeprowadzka na raty!!!), żeby zdobywać sie na tak dociekliwe analizy tematu.

Zatem zapraszam do [kotobusa](https://1.fwcdn.pl/wv/22/49/12249/215732_1.12249.4.jpg) i przechodzę do rzeczy. Konkurs ma bardzo prostą formułę: ja zadaję pytanie, ty wysyłasz odpowiedź na maila konkurs@kot-zrodlowy.pl w temacie wpisując ***"Wygram bilet na ConFrontJS"***. To jest bardzo ważne, bo przy otwieraniu waszych maili nie przepracuje się żaden kot (ani nie ucierpi żadne zwierzę).

### Zadanie konkursowe

Dobrze wiecie, że całym kocim serduchem i wszystkimi łapkami wspieram środowiska developerskie, które tworzą otwarte oprogramowanie i wpływają na to wszystko, czego na co dzień używamy. Wiecie, że sama jestem osobą, która wpływa na kształt naszego JS-owego grajdołka. I musicie sobie uświadomić, że na kształt community wpływa każdy z nas. Dlatego pytanie konkursowe brzmi:

> Z jakim tematem wystąpisz (choćby hipotetycznie) na konferencji dotyczącej szeroko pojętego JS-a?

Na zgłoszenia czekam do 24 października do 23:23, żeby następnie w ciągu 24 godzin zapodać wam zwycięzcę. Wygrywa najciekawsza odpowiedź. Zwycięzca zostanie powiadomiony mailowo, a także udostępniony na kocim blogu i fejsbuku.

## Regulamin konkursu "Wygram bilet na ConFrontJS"
1. Organizatorem konkursu jest administrator bloga Kot Źródłowy.
2. Nagrodą w konkursie jest jeden darmowy bilet ufundowany przez organizatora konferencji ConFrontJS.
3. W konkursie może wziąć udział każda pełnoletnia osoba przebywająca na terenie Polski z wyłączeniem najbliższej rodziny organizatora.
4. Biorąc udział w konkursie wyrażasz zgodę na przetwarzanie swoich danych osobowych do celów przeprowadzenia konkursu.
5. Wybór zwycięzcy odbędzie się za pomocą Jury z złożonego z administratorki strony Kot Źródłowy.
6. Najlepsza odpowiedź zostanie udostępniona na stronie kot-zrodlowy.pl razem z danymi autora, jeśli ten wyrazi na to zgodę.
7. Zwycięzca zostanie powiadomiony drogą mailową o tym fakcie.
8. Konkurs polega na wysłaniu maila o temacie "Wygram bilet na ConFrontJS" na adres konkurs@kot-zrodlowy.pl. W treści wiadomości należy wpisać odpowiedź na pytanie: Z jakim tematem wystąpisz (choćby hipotetycznie) na konferencji dotyczącej szeroko pojętego JS-a?
9. Każdy uczestnik ma prawo do wysłania tylko jednego zgłoszenia. W razie wielu zgłoszeń wysłanych przez tę samą osobę, liczy się tylko pierwsze zgłoszenie.
10. Termin zgłoszeń upływa 24 października o 23:23.
11. Organizator powiadomi o werdykcie do 25 października do godziny 23:23.

Pozostaje mi życzyć Ci powodzenia. Pamiętaj, że to ma być zabawa i wsparcie samej konferencji. Wierzę w Ciebie. To co, widzimy się na ConFrontJS?
Miau! 

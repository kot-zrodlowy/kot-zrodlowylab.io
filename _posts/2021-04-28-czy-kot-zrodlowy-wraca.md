---
layout: post
title: "Czy Kot Źródłowy wraca do gry?"
categories: offtop
---
Cześć!
Oj, dawno mnie tutaj nie było. Sama zastanawiałam się, czy nie zaorać tego miejsca, ale za każdym razem łezka mi się w oku kręciła. (Nie no, żartuję! Nie podejrzewaj mnie o żadne ludzkie uczucia! To tylko kocimiętka.) I tak ostatnio w moim życiu dzieje się tak dużo zmian, że nastała ochota, żeby wrócić. Myślę, że wszystkim nam wyjdzie to na zdrowie. Bo mi da poczucie ciągłego rozwoju, wam więcej wartościowych treści, a internetowi na powrót przybędzie jedno pluszowe  miejsce, w którym wszyscy czują się dobrze. 

### Ale Kot, dlaczego Cię tyle nie było?

Well, to pytanie jest dość trudne. Ale postaram się odpowiedzieć w miarę sprawnie. Jeśli Was to nie interesuje, to skip do następnej sekcji. O, dalej tu jesteście! Super. To, co może o mnie już wiecie i zauważyliście, to jestem kotem wielu talentów i wielu pasji. Ale skoro doba ma tylko 24 godziny, a spać też trzeba (wiedzieliście, że zdrowy kot powinien przesypiać 17h na dobę?), to nie ogarniam wszystkiego naraz, tylko pewne rzeczy przychodzą cyklami. Tak też było z tym blogiem. W końcu, chcąc, nie chcąc, pojawiła się nowa zajawka, a potem kolejna i jeszcze jedna… Mam nadzieję, że rozumiecie mechanizm.

Prawda jest też taka, że kiedy zaczynałam pisać, byłam jeszcze studentką. Nie pracowałam na pełny etat, a, co za tym idzie, miałam na bloga znacznie więcej czasu i pomysłów. Jak może wiecie, swoją karierę wiązałam raczej z korporacjami, więc gdy tylko zaczęłam pracę, musiałam mocno przeorganizować całe życie. Wiąże się z tym jeszcze jedna rzecz: przez ostatnie dwa lata pracowałam przy bardzo ciekawych rzeczach, ale niestety zupełnie nieprzydatnych technologicznie poza moją firmą. Co wiązało się z niedoborem tematów na posty, a nawet, jeśli tematy się znalazły, to pozostawał jeszcze jeden drobiazg.

Musicie coś o mnie wiedzieć. Bywam straszną perfekcjonistką, chociaż pracuję nad tym. Do tego lubię obcować z ładnymi i funkcjonalnymi rzeczami. I tu dochodzimy do szablonu bloga, który się już trochę opatrzył. Poza tym, jako byt napisany w bardzo krótkim czasie, ma pewne niedociągnięcia. Czyli w mojej głowie za każdym razem powstawał plan, żeby przebudować co najmniej szablon bloga od początku. A to już jest grubszy projekt. I tym sposobem, nawet mimo pomysłów na wpisy, widziałam tylko wielkie mury nie do przeskoczenia i mnóstwo ciekawszych rzeczy do roboty, a blog coraz bardziej zarastał mchem i nie ruszał się do przodu nawet o milimetr. 

### Co się zmieniło?

No po prostu zmieniam pracę, otwieram własną firmę i moją działalność blogową staram się w tym wszystkim ująć. Tym bardziej, że sam fakt przechodzenia na swoje pootwierał w mojej głowie zupełnie nowe kierunki rozwoju kota. Mam nadzieję, że się wam spodobają, po prostu. Może zawojuję YT, może stworzę jakieś fajne materiały albo własny kurs. Tego jeszcze nie mogę wam obiecać. Ale jedno jest pewne, będzie się działo!

### Nie wchodzi się łapką dwa razy do tej samej rzeki…

Zacznijmy od tego, że do rzeki w ogóle się nie wchodzi (przecież tam jest woda i można sobie zmoczyć łapki!!!). Ale nie zrozumcie mnie źle, chcę skorzystać z pierwotnego, poprawnego znaczenia tego przysłowia. Chodzi o to, że przez te prawie dwa lata mojej nieobecności wszystko się zmieniło. Ja jestem trochę inna, ale internet też nie stał w miejscu. Co za tym idzie, blog nie będzie taki sam, jak był wcześniej. Kilka zmian mogę na pewno zapowiedzieć teraz, reszta wyjdzie w działaniu. 

Na pewno przestanę ograniczać bloga do suchych tutoriali, a wzbogacę go o różne softskillowe rzeczy, bo to też jest ważne i często bardzo interesujące. Wiem już, jaka wiedza by mi na starcie pomogła, a nie jest tak łatwo dostępna. No i czasami pojawią się wpisy dotyczące moich bardziej analogowych lub mniej technicznych pasji i tego, jaki mają wpływ na codzienną pracę. Nie chcę się ograniczać, bo to u mnie nie działa. I tak, szablon też się zmieni, ale powolutku, bez spiny, w zgodzie ze mną. W końcu nie można sobie tego bez końca prokrastynować!

Mam nadzieję, że będziecie chcieli aktywnie uczestniczyć w tych rewolucjach i spodobają wam się ich wyniki. Pamiętajcie, że zawsze możecie pójść w inny kawałek internetu. Tymczasem trzymajcie się ciepło i dbajcie o siebie!
Miau!
